package com.atrams.tullow.users

import com.atrams.tullow.utils.Config
import com.parse.ParseObject

/**
 * Created by yaaminu on 6/8/17.
 */
data class User(val name: String,
                val userId: String,
                val phoneNumber: String,
                val isVerified: Boolean = false,
                val dp: String?,
                val vehicleNo: String) {

    companion object {
        @JvmStatic
        fun create(parseUser: ParseObject?): User? {
            if (parseUser == null) {
                return null
            }
            var dp = parseUser.getString("dp")
            if (dp == null) {
                dp = Config.getApplicationWidePrefs()
                        .getString("current.user.dp.local", null)
            }
            val user = User(parseUser.getString("name"), parseUser.objectId, parseUser.getString("phone_number"),
                    parseUser.getBoolean("verified"),
                    dp, parseUser.getString("vehicleNo"))
            return user
        }

    }
}