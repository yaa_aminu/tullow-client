package com.atrams.tullow.users

/**
 * Created by yaaminu on 6/8/17.
 */
interface UserApi {
    fun login(name: String, phoneNumber: String, vehicleNo: String): rx.Observable<User>
    fun verify(verificationCode: String): rx.Observable<User>
    fun getCurrentUser(): User?
    fun isCurrentUserVerified(): Boolean
    fun isCurrentUserSetup(): Boolean
    fun isUserLoggedIn(): Boolean
    fun logout(): rx.Observable<Any>
}