package com.atrams.tullow

import butterknife.ButterKnife
import com.atrams.tullow.users.UserManager
import com.atrams.tullow.utils.Config
import com.atrams.tullow.utils.PLog
import com.atrams.tullow.utils.Task
import com.atrams.tullow.utils.TaskManager
import com.birbit.android.jobqueue.JobManager
import com.birbit.android.jobqueue.config.Configuration
import io.realm.Realm
import io.realm.RealmConfiguration
import org.jetbrains.anko.doAsync

/**
 * Created by yaaminu on 6/8/17.
 */
class MoveRifft : android.app.Application() {

    lateinit var jobManager: com.birbit.android.jobqueue.JobManager

    override fun onCreate() {
        super.onCreate()
        ButterKnife.setDebug(BuildConfig.DEBUG)
        Realm.init(this)
        val configBuilder = RealmConfiguration.Builder()
                .deleteRealmIfMigrationNeeded()
                .build()
        Realm.setDefaultConfiguration(configBuilder)
        jobManager = JobManager(Configuration.Builder(this).build());
        PLog.setLogLevel(if (BuildConfig.DEBUG) PLog.LEVEL_VERBOSE else PLog.LEVEL_ERROR)
        Config.init(this)
        UserManager.initialize(this)
        TaskManager.init(jobRunner)
    }

    val jobRunner = object : TaskManager.JobRunner {
        override fun runJobBlocking(task: Task): String {
            jobManager.addJob(task)
            return task.id
        }

        override fun cancelJobsAsync(tag: String) {
            doAsync {
                jobManager.cancelJobs(com.birbit.android.jobqueue.TagConstraint.ALL, tag)
            }
        }

        override fun startAsync() {
            doAsync {
                jobManager.start()
            }
        }
    }
}