package com.atrams.tullow.users;

import android.content.Context;

import com.atrams.tullow.BuildConfig;
import com.atrams.tullow.net.ParseBackend;
import com.atrams.tullow.utils.Config;
import com.atrams.tullow.utils.GenericUtils;
import com.parse.ParseInstallation;
import com.parse.ParseObject;
import com.parse.ParseUser;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import rx.Observable;
import rx.functions.Func1;

/**
 * Created by yaaminu on 8/17/17.
 */

public class UserManager implements UserApi {


    public static final String KEY_SETUP = "setup";
    private final ParseBackend backend;

    public UserManager(ParseBackend backend) {
        this.backend = backend;
    }

    @NotNull
    @Override
    public Observable<User> login(@NotNull String name, @NotNull String phoneNumber,
                                  @Nullable final String vehicleNo) {

        return backend.loginOrSignup(name, phoneNumber)
                .map(new Func1<ParseObject, User>() {
                    @Override
                    public User call(ParseObject parseObject) {
                        return User.create(parseObject);
                    }
                });
    }

    @NotNull
    @Override
    public Observable<User> verify(@NotNull String verificationCode) {
        return backend.verify(verificationCode)
                .map(new Func1<ParseObject, User>() {
                    @Override
                    public User call(ParseObject parseObject) {
                        return User.create(parseObject);
                    }
                });
    }

    @Nullable
    @Override
    public User getCurrentUser() {
        return User.create(backend.getCurrentUser());
    }

    @Override
    public boolean isCurrentUserVerified() {
        User user = getCurrentUser();
        return user != null && user.isVerified();
    }

    @Override
    public boolean isCurrentUserSetup() {
        if (!isCurrentUserVerified()) {
            return false;
        }
        final User currentUser = getCurrentUser();
        GenericUtils.assertThat(currentUser != null, "must not be null");
        return BuildConfig.FLAVOR.equals(BuildConfig.FLAVOR_NAME_CLIENT) && Config.getApplicationWidePrefs().getBoolean(KEY_SETUP, false);
    }

    @Override
    public boolean isUserLoggedIn() {
        return getCurrentUser() != null;
    }

    @NotNull
    @Override
    public Observable<Object> logout() {
        return backend.logout();
    }


    public Observable<User> updateCurrentUserDp(String path) {
        return backend.updateCurrentUserDp(path);
    }

    public static void initialize(Context context) {
        com.parse.Parse.initialize(new com.parse.Parse.Configuration.Builder(context)
                .server(BuildConfig.SERVER_URL)
                .enableLocalDataStore()
                .applicationId(BuildConfig.APP_ID)
                .build());
        if (ParseUser.getCurrentUser() != null) {
            ParseInstallation.getCurrentInstallation().saveInBackground();
        }
        com.parse.Parse.setLogLevel(com.parse.Parse.LOG_LEVEL_VERBOSE);
    }

    static class Holder {
        static UserManager instance = new UserManager(ParseBackend.getInstance());
    }

    public static UserManager getInstance() {
        return Holder.instance;
    }
}
