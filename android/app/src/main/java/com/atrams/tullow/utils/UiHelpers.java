package com.atrams.tullow.utils;

import android.app.ProgressDialog;
import android.content.Context;
import android.support.v7.app.AlertDialog;
import android.widget.Toast;

import com.atrams.tullow.R;


/**
 * Created by yaaminu on 12/20/16.
 */
public class UiHelpers {
    public static void showToast(String toast) {
        Toast.makeText(Config.getApplicationContext(), toast, Toast.LENGTH_LONG).show();
    }

    public static void showErrorDialog(Context context, String message) {
        new AlertDialog.Builder(context)
                .setMessage(message)
                .setTitle("Error")
                .setPositiveButton(android.R.string.ok, null)
                .create().show();
    }

    public static ProgressDialog showProgressDialog(Context context) {
        ProgressDialog progressDialog = new ProgressDialog(context);
        progressDialog.setCancelable(false);
        progressDialog.setMessage(GenericUtils.getString(R.string.please_wait));
        progressDialog.show();
        return progressDialog;
    }
}
