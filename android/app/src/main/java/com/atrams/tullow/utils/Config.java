package com.atrams.tullow.utils;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Environment;

import com.atrams.tullow.R;

import java.io.File;

import static android.os.Environment.getExternalStoragePublicDirectory;

/**
 * @author null-pointer
 */
@SuppressWarnings("WeakerAccess")
public class Config {

    public static final String APP_PREFS = "prefs";
    private static final String TAG = Config.class.getSimpleName();

    private static final String logMessage = "calling getApplication when init has not be called";
    private static final String detailMessage = "application is null. Did you forget to call Config.init()?";
    private static String APP_NAME = "Moverifft";
    private static Application application;

    private static boolean isExternalStorageAvailable() {
        String state = Environment.getExternalStorageState();
        return state.equals(Environment.MEDIA_MOUNTED);
    }

    public static void init(Application pairApp) {
        Config.application = pairApp;
        setUpDirs();
    }


    @SuppressWarnings("ResultOfMethodCallIgnored")
    private static void setUpDirs() {
        if (isExternalStorageAvailable()) {
            //no need to worry calling this several times
            //if the file is already a directory it will fail silently
            getAppBinFilesBaseDir().mkdirs();
            getTempDir().mkdirs();
        } else {
            PLog.w(TAG, "This is strange! no sdCard available on this device");
        }

    }


    public static Context getApplicationContext() {
        if (application == null) {
            warnAndThrow(logMessage, detailMessage);
        }
        return application.getApplicationContext();
    }

    public static Application getApplication() {
        if (application == null) {
            warnAndThrow(logMessage, detailMessage);
        }
        return Config.application;
    }

    private static void warnAndThrow(String msg, String detailMessage) {
        PLog.w(TAG, msg);
        throw new IllegalStateException(detailMessage);
    }


    public static SharedPreferences getApplicationWidePrefs() {
        if (application == null) {
            throw new IllegalStateException("application is null,did you forget to call init(Context) ?");
        }
        return getPreferences(Config.APP_PREFS);
    }

    public static File getAppBinFilesBaseDir() {
        File file = new File(Environment.getExternalStoragePublicDirectory(APP_NAME), getApplicationContext()
                .getString(R.string.folder_name_files));

        if (!file.isDirectory()) {
            if (!file.mkdirs()) {
                PLog.f(TAG, "failed to create files dir");
            }
        }
        return file;
    }


    public static File getTempDir() {
        File file = new File(
                getExternalStoragePublicDirectory(APP_NAME), "TMP");
        if (!file.isDirectory()) {
            if (!file.mkdirs()) {
                PLog.f(TAG, "failed to create tmp dir");
            }
        }
        return file;
    }

    public static SharedPreferences getPreferences(String s) {
        return getApplicationContext().getSharedPreferences(s, Context.MODE_PRIVATE);
    }

}
