package com.atrams.tullow.utils;

import android.support.annotation.Nullable;
import android.view.View;

/**
 * @author Null-Pointer on 9/20/2015.
 */
public class ViewUtils {

    public static boolean isViewVisible(@Nullable View view) {
        return view != null && view.getVisibility() == View.VISIBLE;
    }

    private static void hideViewInternal(View view) {
        if (isViewVisible(view)) {
            view.setVisibility(View.GONE);
        }
    }

    private static void showViewInternal(@Nullable View view) {
        if (view != null && !isViewVisible(view)) {
            view.setVisibility(View.VISIBLE);
        }
    }

    public static void hideViews(View... views) {
        for (View view : views) {
            hideViewInternal(view);
        }
    }

    public static void showViews(View... views) {
        for (View view : views) {
            showViewInternal(view);
        }
    }

    public static void toggleVisibility(@Nullable View view) {
        if (view != null) {
            view.setVisibility(view.getVisibility() == View.GONE ? View.VISIBLE : View.GONE);
        }
    }

    public static void showByFlag(boolean show, @Nullable View view) {
        if (view == null) return;
        view.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    public static void enableViews(View v, View... more) {
        enableOrDisable(true, v, more);
    }

    public static void disableViews(View v, View... more) {
        enableOrDisable(false, v, more);
    }

    public static void enableByFlag(boolean flag, View view, View... others) {
        enableOrDisable(flag, view, others);
    }

    private static void enableOrDisable(boolean enable, View v, View[] more) {
        if (v != null) {
            v.setEnabled(enable);
        }
        for (View view : more) {
            if (view != null) {
                view.setEnabled(enable);
            }
        }
    }
}
