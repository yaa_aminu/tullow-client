package com.atrams.tullow.net;

import com.atrams.tullow.users.User;
import com.parse.ParseObject;
import com.parse.ParseUser;

import org.jetbrains.annotations.NotNull;

import rx.Observable;


/**
 * Created by yaaminu on 6/8/17.
 */

public class ParseBackend {
    private static final String TAG = "ParseBackend";
    private static final String FIELD_NAME_TRUCKS = "truck";

    @NotNull
    public static ParseBackend getInstance() {
        return new ParseBackend();
    }

    public Observable<ParseObject> loginOrSignup(String name, String phoneNumber) {
        return Observable.error(new Exception("failed"));
    }

    public Observable<ParseObject> verify(String verificationCode) {
        return Observable.error(new Exception("failed"));
    }

    public ParseObject getCurrentUser() {
        return ParseUser.getCurrentUser();
    }

    public Observable<Object> logout() {
        return Observable.error(new Exception("failed"));
    }

    public Observable<User> updateCurrentUserDp(String path) {
        return Observable.error(new Exception("failed"));
    }
}
