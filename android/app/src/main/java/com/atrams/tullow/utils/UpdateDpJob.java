package com.atrams.tullow.utils;

import android.support.annotation.NonNull;

import com.atrams.tullow.net.ParseBackend;
import com.birbit.android.jobqueue.Params;
import com.birbit.android.jobqueue.RetryConstraint;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;

import static com.birbit.android.jobqueue.RetryConstraint.createExponentialBackoff;
import static java.util.concurrent.TimeUnit.SECONDS;

/**
 * Created by yaaminu on 12/26/17.
 */

public class UpdateDpJob extends Task {

    private static final String TAG = "UpdateDpJob";
    @NonNull
    private String dp;

    ParseUser currentUser;

    //required no arg
    @SuppressWarnings("unused")
    public UpdateDpJob() {
        this.dp = "";
    }

    public UpdateDpJob(Params params, @NonNull String dp, ParseUser parseUser) {
        super(params);
        this.dp = dp;
        this.currentUser = parseUser;
    }

    public static UpdateDpJob create(String dp, ParseUser parseUser) {
        Params params = new Params(100);
        params.setRequiresNetwork(true);
        params.setPersistent(true);
        params.setGroupId("dp");
        return new UpdateDpJob(params, dp, parseUser);
    }

    @Override
    protected JSONObject toJSON() {
        try {
            return new JSONObject()
                    .put("dp", this.dp);
        } catch (JSONException e) {
            throw new RuntimeException();
        }
    }

    @Override
    protected Task fromJSON(JSONObject jsonObject) {
        try {
            return create(jsonObject.getString("dp"), (ParseUser) ParseBackend.getInstance()
                    .getCurrentUser());
        } catch (JSONException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void onRun() throws Throwable {
        ParseFile parseFile = new ParseFile(new File(dp), FileUtils.getMimeType(dp));
        Config.getApplicationWidePrefs().edit()
                .putString("current.user.dp.local", dp)
                .apply();
        parseFile.save();
        currentUser.put("dp", parseFile.getUrl());
        currentUser.save();
        ParseObject parseObject = ParseQuery.getQuery("truck")
                .whereEqualTo("ownerId", currentUser.getObjectId())
                .getFirst();
        if (parseObject != null) {
            parseObject.put("ownerDp", parseFile.getUrl());
            parseObject.put("dp", new File(dp).getAbsolutePath());
            parseObject.save();
        }
    }

    @Override
    protected RetryConstraint shouldReRunOnThrowable(@NonNull Throwable throwable, int runCount, int maxRunCount) {
        return createExponentialBackoff(runCount, SECONDS.toMillis(30));
    }
}
