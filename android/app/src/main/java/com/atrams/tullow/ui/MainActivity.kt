package com.atrams.tullow.ui

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.atrams.tullow.R

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
}
