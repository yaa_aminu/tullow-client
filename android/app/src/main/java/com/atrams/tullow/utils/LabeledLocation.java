package com.atrams.tullow.utils;

import android.graphics.Bitmap;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

/**
 * Created by yaaminu on 8/24/17.
 */
public class LabeledLocation implements Parcelable {

    @NonNull
    public final String label;
    public final double lat;
    public final double lng;
    @Nullable
    public final Bitmap marker;
    @NonNull
    public final String tag;

    public LabeledLocation(@NonNull LatLang latLang, @NonNull String label,
                           @Nullable Bitmap marker, @NonNull String tag) {
        this.label = label;
        this.lat = latLang.getLatitude();
        this.lng = latLang.getLongitude();
        this.marker = marker;
        this.tag = tag;
    }

    protected LabeledLocation(Parcel in) {
        label = in.readString();
        lat = in.readDouble();
        lng = in.readDouble();
        marker = in.readParcelable(Bitmap.class.getClassLoader());
        tag = in.readString();
    }

    public static final Creator<LabeledLocation> CREATOR = new Creator<LabeledLocation>() {
        @Override
        public LabeledLocation createFromParcel(Parcel in) {
            return new LabeledLocation(in);
        }

        @Override
        public LabeledLocation[] newArray(int size) {
            return new LabeledLocation[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(label);
        parcel.writeDouble(lat);
        parcel.writeDouble(lng);
        parcel.writeParcelable(marker, i);
        parcel.writeString(tag);
    }


}
