package com.atrams.tullow.utils;

import android.location.Address;
import android.support.annotation.NonNull;
import android.text.TextUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by yaaminu on 9/24/17.
 */

public class LocationUtils {
    public static String summariseAddress(@NonNull Address address) {
        //noinspection StringBufferReplaceableByString
        final List<String> builder = new ArrayList<>(5);
        if (address.getLocality() != null) {
            builder.add(address.getLocality());
        }
        if (address.getSubAdminArea() != null && !address.getSubAdminArea().equals(address.getLocality())) {
            builder.add(address.getSubAdminArea());
        }
        if (address.getAdminArea() != null && !address.getAdminArea().equals(address.getSubAdminArea())) {
            builder.add(address.getAdminArea());
        }

        return TextUtils.join(", ", builder);
    }
}
