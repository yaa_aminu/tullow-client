package com.atrams.tullow.utils;

import android.content.Context;
import android.location.Location;
import android.location.LocationManager;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.DrawableRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.google.android.gms.maps.model.LatLng;
import com.parse.ParseGeoPoint;

import org.json.JSONException;
import org.json.JSONObject;

import io.realm.RealmObject;

/**
 * Created by yaaminu on 8/22/17.
 */

public class LatLang extends RealmObject implements Parcelable {
    private double latitude;
    private double longitude;

    public LatLang() {

    }

    public LatLang(double latitude, double longitude) {
        this.latitude = latitude;
        this.longitude = longitude;
    }

    protected LatLang(Parcel in) {
        this.latitude = in.readDouble();
        this.longitude = in.readDouble();
    }

    public static final Creator<LatLang> CREATOR = new Creator<LatLang>() {
        @Override
        public LatLang createFromParcel(Parcel in) {
            return new LatLang(in);
        }

        @Override
        public LatLang[] newArray(int size) {
            return new LatLang[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeDouble(getLatitude());
        parcel.writeDouble(getLongitude());
    }

    public static LatLang from(Location location) {
        return new LatLang(location.getLatitude(), location.getLongitude());
    }

    public static LatLang from(LatLng latLng) {
        return new LatLang(latLng.latitude, latLng.longitude);
    }

    @Nullable
    public static LatLang from(@Nullable ParseGeoPoint location) {
        if (location == null) {
            return null;
        }
        return new LatLang(location.getLatitude(), location.getLongitude());
    }

    public double distanceInMetersTo(@NonNull LatLang destination) {
        return toLocation().distanceTo(destination.toLocation());
    }

    public LatLng toLatLng() {
        return new LatLng(getLatitude(), getLongitude());
    }

    public Location toLocation() {
        Location location = new Location(LocationManager.GPS_PROVIDER);
        location.setLatitude(getLatitude());
        location.setLongitude(getLongitude());
        return location;
    }

    @Override
    public String toString() {
        return "LatLang{" +
                "latitude=" + getLatitude() +
                ", longitude=" + getLongitude() +
                '}';
    }

    public LabeledLocation toLabeledLocation(Context context, String label,
                                             @DrawableRes int marker, String tag) {
        return new LabeledLocation(this, label,
                marker == 0 ? null :
                       GenericUtils.vectorToDrawable(context, marker), tag);
    }

    public double getLatitude() {
        return latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public JSONObject toJson() throws JSONException {
        return new JSONObject().put("lat", getLatitude())
                .put("long", getLongitude());
    }

    public static LatLang fromJson(String jsonText) throws JSONException {
        final JSONObject jsonObject = new JSONObject(jsonText);
        return new LatLang(jsonObject.getDouble("lat"), jsonObject.getDouble("long"));
    }

    public static LatLng centroid(LatLang... latLangs) {
        double lat = 0.00;
        double lng = 0.00;
        for (LatLang other : latLangs) {
            lng += other.longitude;
            lat += other.latitude;
        }
        return new LatLng(lat / latLangs.length, lng / latLangs.length);
    }
}
